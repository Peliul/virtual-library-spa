import { Routes } from '@angular/router';
import { NavbarComponent } from './app/components/navbar/navbar.component';
import { BooksComponent } from './app/components/navbar/books/books.component';
import { BooksResolver } from './app/_resolvers/books.resolver';

export const appRoutes: Routes = [
    { path: '', component: BooksComponent, resolve: {books: BooksResolver} },
    { path: 'books', component: BooksComponent, resolve: {books: BooksResolver}},
    { path: '**', redirectTo: 'books', pathMatch: 'full' }
];