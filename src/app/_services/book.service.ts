import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaginatedResult } from '../_models/pagination';
import { Book } from '../_models/book';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getBooks(page?, itemsPerPage?, userParams?): Observable<PaginatedResult<Book[]>> {
    const paginatedResult: PaginatedResult<Book[]> = new PaginatedResult<Book[]>();

    let params = new HttpParams();

    if(page != null && itemsPerPage != null) {
      params = params.append('pageNumber', page);
      params = params.append('pageSize', itemsPerPage);
    }

    if(userParams != null) {
      params = params.append('genue', userParams.genue);
      params = params.append('author', userParams.author);
    }

    return this.http.get<Book[]>(this.baseUrl + 'books', { observe: 'response', params})
      .pipe(
        map(response => {
          paginatedResult.result = response.body;
          if(response.headers.get('Pagination') != null) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        })
      );
  }
}
