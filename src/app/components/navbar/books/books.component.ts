import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models/user';
import { Pagination, PaginatedResult } from 'src/app/_models/pagination';
import { BookService } from 'src/app/_services/book.service';
import { ActivatedRoute } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Book } from 'src/app/_models/book';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  books: Book[];
  userParams: any = {};
  pagination: Pagination;

  constructor(private bookService: BookService, private route: ActivatedRoute, private alertify: AlertifyService) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.books = data['books'].result;
      this.pagination = data['books'].pagination;
    });

    this.userParams.genue = 'All';
    this.userParams.author = 'All';
  }

  pageChanged(event: any): void {
    this.pagination.currentPage = event.page;
    this.loadBooks();
  }

  filter(genue, author) {
    //this.userParams.genue = genue;
    //this.userParams.author = author;
    console.log('genue', this.userParams.genue, ' author', this.userParams.author);
    this.loadBooks();
  }

  loadBooks() {
    this.bookService.getBooks(this.pagination.currentPage, this.pagination.itemsPerPage, this.userParams)
      .subscribe(
        (res: PaginatedResult<Book[]>) => {
          this.books = res.result;
          this.pagination = res.pagination;
        }, error => {
          this.alertify.error(error);
        }
      )
  }
}
