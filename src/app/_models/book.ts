export interface Book {
    id: number;
    title: string;
    author: string;
    publishedAt?: Date;
    publishers?: string;
    ISBN?: string;
    genue: string;
    coverUrl?: string;
}