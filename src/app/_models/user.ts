import { Book } from './book';

export interface User {
    id: number;
    username: string;
    created: Date;
    books?: Book[];
}