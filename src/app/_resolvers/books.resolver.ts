import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Book } from '../_models/book';
import { BookService } from '../_services/book.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertifyService } from '../_services/alertify.service';

@Injectable()
export class BooksResolver implements Resolve<Book[]> {
    pageNumber = 1;
    pageSize = 18;
    userParams = {
        genue: 'All',
        author: 'All'
    };

    constructor(private bookService: BookService, private router: Router, private alertify: AlertifyService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Book[]> {
        return this.bookService.getBooks(this.pageNumber, this.pageSize, this.userParams)
            .pipe(
                catchError(error => {
                    this.alertify.error('Problem retriving data');
                    //this.router.navigate(['/books']);
                    return of(null);
                })
            );
    }
}